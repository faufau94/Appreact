import React from 'react';
import {View,Text,Button,StatusBar} from 'react-native';
import { DrawerNavigator } from 'react-navigation';
import firstscreen from './Screens/firstscreen';
import secondscreen from './Screens/secondscreen';


export class Home extends React.Component {
  render() {
    return (
      <View>
      <StatusBar hidden = {true}/>
      	<Text>okkkkkk</Text> 
      	<Button
        title = 'ouvre le Drawer'
        onPress = {() => this.props.navigation.navigate('DrawerOpen')}
        />        
      </View>
    );
  }
}

export default DrawerNavigator({
	back: {
		screen: Home
	},
  premier: {
    screen: firstscreen
  },
  deuxieme: {
    screen: secondscreen
  }
})
