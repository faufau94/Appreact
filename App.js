import React from 'react';
import {
  View,
  Text,
  Button,
  StatusBar
} from 'react-native';
import { StackNavigator } from 'react-navigation';
import Home from './components/Home';


export class App extends React.Component {
  static navigationOptions = {
    header: null
  }

  render() {
    return (
     <View>
    <StatusBar hidden = {true}/>
      <Text>Bonjour ! </Text>
      <Button
      title = 'Press me'
      onPress = {() => this.props.navigation.navigate('Home')}
      />
    </View>
    );
  }
}

export default StackNavigator({
  App: {
    screen: App
  },
  Home: {
    screen: Home
  }
});


